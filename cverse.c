#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int linecount(FILE *infile , int lindex);
void randline(FILE *infile , int lindex);

int main (int argc , char **argv) {
	srand((unsigned) time(NULL));

	if (argc > 2) {
		fprintf(stderr, "cverse: too many arguments\n");

		return 1;
	} else if (argc < 2) {
		fprintf(stderr, "cverse: missing file operand\n");

		return 1;
	}

	FILE *infile = fopen(argv[1], "r");
	if (infile == NULL) {
		fprintf(stderr, "cverse: file is empty\n");

		return 2;
	}

	int lines = linecount(infile, RAND_MAX);

	randline(infile, rand() % lines);

	fclose(infile);

	return 0;
}

int linecount (FILE *infile , int lindex) {
	int lcount = 0;
	int prev = '\n';
	int inchr;

	rewind(infile);
	while (lindex > 0 && (inchr = fgetc(infile)) != EOF) {
		if (inchr == '\n') {
			lindex--;
		}
		if (prev == '\n') {
			lcount++;
		}
		prev = inchr;
	}

	return lcount;
}

void randline (FILE *infile , int lindex) {
	linecount(infile, lindex);

	int inchr;
	while ((inchr = fgetc(infile)) != EOF && inchr != '\n') {
		putchar(inchr);
	}
	printf("\n");
}
